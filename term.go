package main

import (
	"fmt"
	termbox "github.com/nsf/termbox-go"
	"github.com/nsf/tulib"
	"github.com/sgeb/go-tuikit/tuikit"
	"sort"
	"strconv"
	"time"
)

const (
	clientFormat = "%5v  %-4s  %-5s"
)

var (
	header string
)

type window struct {
	*tuikit.BaseView

	clientsLabel *tuikit.TextView
	clients      *tuikit.TextView
	headerBg     *tuikit.FillerView
	headerFg     *tuikit.TextView
	clientsList  *tuikit.LinearLayout
	blank        *tuikit.BlankView
	numClients   int
}

func runTerm() {
	if err := tuikit.Init(); err != nil {
		panic(err)
	}
	defer tuikit.Close()

	w := newWindow()
	tuikit.SetPainter(w)

	ticker := time.Tick(time.Second)

	for {
		select {
		case ev := <-tuikit.Events:
			if ev.Type == termbox.EventKey && ev.Ch == 'q' {
				return
			}

		case <-ticker:
			func() {
				clients := server.Clients()

				now := time.Now().Unix()

				ids := make([]int, 0, len(clients))
				for cid := range clients {
					ids = append(ids, int(cid))
				}
				sort.Ints(ids)

				clientPainters := make([]tuikit.Painter, 0)
				for _, cid := range ids {
					client := clients[int32(cid)]
					tv := tuikit.NewTextView()
					tv.SetText(fmt.Sprintf(clientFormat, cid, client.Name, client.Level))
					if client.LastDiscovery <= now-5 {
						tv.SetParams(&tulib.LabelParams{
							Fg: termbox.ColorRed,
						})
					}
					clientPainters = append(clientPainters, tv)
				}
				w.updateClientsList(clientPainters)
				w.clients.SetText(strconv.Itoa(len(clients)))
			}()
		}
	}
}

func newWindow() *window {
	w := &window{
		BaseView:     tuikit.NewBaseView(),
		clientsLabel: tuikit.NewTextView(),
		clients:      tuikit.NewTextView(),
		headerBg:     tuikit.NewFillerView(termbox.Cell{Ch: ' ', Bg: termbox.ColorWhite}),
		headerFg:     tuikit.NewTextView(),
		clientsList:  tuikit.NewLinearLayout(nil),
		blank:        tuikit.NewBlankView(),
	}
	w.clientsLabel.SetText("Clients: ")
	w.clients.SetText("0")
	header = fmt.Sprintf(clientFormat, "ID", "Name", "Level")
	w.headerFg.SetText(header)
	w.headerFg.SetParams(&tulib.LabelParams{Fg: termbox.ColorBlack, Bg: termbox.ColorWhite})
	w.SetUpdateChildrenRect(w.updateChildrenRect)
	return w
}

func (w *window) updateChildrenRect(rect tuikit.Rect) error {
	w.AttachChild(w.clientsLabel, tuikit.NewRect(0, 0, rect.Width, 1))
	w.AttachChild(w.clients, tuikit.NewRect(9, 0, rect.Width, 1))
	w.AttachChild(w.headerBg, tuikit.NewRect(0, 2, rect.Width, 2))
	w.AttachChild(w.headerFg, tuikit.NewRect(0, 2, len(header), 2))
	w.AttachChild(w.clientsList, tuikit.NewRect(0, 3, rect.Width, rect.Height))
	w.AttachChild(w.blank, tuikit.NewRect(0, 3+w.numClients, rect.Width, rect.Height))
	return nil
}

func (w *window) updateClientsList(list []tuikit.Painter) {
	w.clientsList.NeedResize()
	w.DetachChild(w.clientsList)
	w.numClients = len(list)
	w.clientsList = tuikit.NewLinearLayout(list)
}
