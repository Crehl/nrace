package main

import (
	"bitbucket.org/Crehl/nrtk/mp"
	"flag"
)

const (
	SlowPort  = 64502
	RapidPort = 64503
)

var (
	clientId        int32 = 1660713
	serverIp        string
	server          *mp.Server
	blueprintGhosts map[string]string
)

func init() {
	flag.StringVar(&serverIp, "IP", "", "The IP address of the server")
	flag.Parse()

	blueprintGhosts = map[string]string{
		"CarNetDummy":        "CarGhost",
		"VanNetDummy":        "VanGhost",
		"CarNetDummy_Gold":   "CarGhost_Gold",
		"DeloreanNetDummy":   "DeloreanGhost",
		"ImpulseCarNetDummy": "ImpulseCarGhost",
	}
}

func main() {
	updateChan := make(chan mp.UpdateMessage, 5)
	server = mp.NewServer(clientId, nil, updateChan)

	err := server.StartListening(serverIp, mp.DiscoveryPort, mp.UpdatePort)
	if err != nil {
		panic(err)
	}

	go func() {
		for {
			msg := <-updateChan
			cid := msg.GetClientId()
			msg.ClientId = &clientId

			for _, obj := range msg.GetObjectData() {
				if ghost, ok := blueprintGhosts[obj.GetBlueprint()]; ok {
					obj.Blueprint = ghost
				}
			}

			server.BroadcastUpdateFromClient(&msg, cid)
		}
	}()

	runTerm()
}
