# README #

NRace is a server/host for Nitronic Rush multiplayer over the internet. Since update #5, NR has support for automatic multiplayer over LAN, but multiplayer over the internet requires knowing every player's external IP, and port forwarding (where necessary) of UDP ports 64502 and 64503. NRace is a solution to these issues by acting as a relay that forwards messages from all connected clients to each other. NRace should be run on a machine with a fixed IP and the above ports open, but clients have no such restrictions - they need only know the server's IP. Clients without a fixed IP or with NATs that perform Port Address Translation are still able to connect without problems.

# Joining an NRace Server #

Joining is simple, simply edit `Nitronic Rush\config\core_config.ini` and add a line at the end: `ExtraAddresses=<Server IP>,` (note the trailing comma, and make sure the line doesn't begin with a semicolon), replacing the server IP as necessary. Note that NRace works slightly differently to NR, so adding your external IP address isn't required (and is in fact ignored anyway). The next time you start NR you should see anyone else connected who's currently on the same level.

# Running an NRace Server #

Running a server involves little more than getting a copy of the repository and compiling (assuming you already have [Go](http://golang.org) installed):

* `go get -u bitbucket.org/Crehl/nrace`
* `"$GOPATH/bin/nrace" -IP="<Server IP>"` (Linux) OR `"%GOPATH%\bin\nrace" -IP="<Server IP>"` (Windows)